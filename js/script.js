//for test
/*const users1 = [{
  name: "Ivan",
  surname: "Ivanov",
  gender: "male",
  age: 30
},
{
    name: "Anna",
    surname: "Ivanova",
    gender: "female",
    age: 22
},
{
    name: "An",
    surname: "Ivanova",
    gender: "female",
    age: 22
},
{
  name: "Ivanka",
  surname: "Ivanov",
  gender: "male",
  age: 30
}]
const users2 = [{
  name: "Ivan",
  surname: "Ivanov",
  gender: "male",
  age: 30
},
{
    name: "Anna",
    surname: "Ivanova",
    gender: "female",
    age: 21
},
{
  name: "Ivanka",
  surname: "Ivanov",
  gender: "male",
  age: 30
}];

const arr = excludeBy(users1, users2, 'name');
console.log(arr);*/

function excludeBy(peopleList, excluded, prop){
	return peopleList.filter(function(ob1){
		return !excluded.some(function(ob2){
			 return ob1[prop] == ob2[prop];
		});
	});
}